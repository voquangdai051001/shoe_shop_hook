import React from "react";

export default function CartShoe({
  cartShoe,
  handleDelete,
  handleChangequantity,
}) {
  let renderCartShoe = () => {
    return cartShoe.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>
            <img style={{ width: "100px" }} src={item.image}></img>
          </td>
          <td style={{ fontWeight: "500" }}>{item.name}</td>

          <td>
            <button
              onClick={() => {
                handleChangequantity(item.id, 1);
              }}
              className="btn btn-success p-1"
            >
              +
            </button>
            <strong>{item.soLuong}</strong>
            <button
              onClick={() => {
                handleChangequantity(item.id, -1);
              }}
              className="btn btn-danger p-1"
            >
              -
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                handleDelete(item.id);
              }}
              className="btn text-danger border-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Image</th>
            <th>Name</th>
            <th>Quantity</th>
            <th></th>
          </tr>
        </thead>
        <tbody>{renderCartShoe()}</tbody>
      </table>
    </div>
  );
}
