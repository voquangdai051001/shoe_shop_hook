import React, { useState } from "react";
import CartShoe from "./CartShoe";
import { dataShoeArr } from "./DataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default function ShoeShop() {
  const [shoeArr, setShoeArr] = useState(dataShoeArr);
  const [cartShoe, setCartShoe] = useState([]);
  const [detailShoe, setDetailShoe] = useState(shoeArr[0]);

  let handleViewDetail = (value) => {
    setDetailShoe(value);
  };
  let handleAddToCart = (value) => {
    let cloneCart = [...cartShoe];

    let index = cloneCart.findIndex((item) => {
      return item.id == value.id;
    });
    if (index == -1) {
      let newShoe = { ...value, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }

    setCartShoe(cloneCart);
  };
  let handleDelete = (id) => {
    let cloneCart = [...cartShoe];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    cloneCart.splice(index, 1);
    setCartShoe(cloneCart);
  };
  let handleChangequantity = (id, luaChon) => {
    let cloneCart = [...cartShoe];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    cloneCart[index].soLuong += luaChon;
    cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
    setCartShoe(cloneCart);
  };
  return (
    <div>
      ,
      <div className="row">
        <div className="col-6">
          <CartShoe
            cartShoe={cartShoe}
            handleDelete={handleDelete}
            handleChangequantity={handleChangequantity}
          />
        </div>
        <div className="col-6">
          <ListShoe
            shoeArr={shoeArr}
            handleViewDetail={handleViewDetail}
            handleAddToCart={handleAddToCart}
          />
        </div>
      </div>
      <DetailShoe detailShoe={detailShoe} />
    </div>
  );
}
