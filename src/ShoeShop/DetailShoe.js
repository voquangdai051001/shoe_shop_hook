import React from "react";

export default function DetailShoe({ detailShoe }) {
  let { image, name, price, description, shortDescription } = detailShoe;
  return (
    <div className="container">
      <div className="card">
        <img
          style={{ width: "200px", display: "block", margin: "auto" }}
          className="card-img-top"
          src={image}
          alt="Card image cap"
        />
        <div className="card-body">
          <h4 className="card-title">{name}</h4>
          <p className="card-text">{price}</p>
        </div>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">{description}</li>
          <li className="list-group-item">{shortDescription}</li>
        </ul>
      </div>
    </div>
  );
}
