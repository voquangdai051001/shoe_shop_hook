import React from "react";
import ItemShoe from "./ItemShoe";

export default function ListShoe({
  shoeArr,
  handleViewDetail,
  handleAddToCart,
}) {
  let renderListShoe = () => {
    return shoeArr.map((item, index) => {
      return (
        <ItemShoe
          key={index}
          dataItem={item}
          handleViewDetail={handleViewDetail}
          handleAddToCart={handleAddToCart}
        />
      );
    });
  };
  return <div className="row">{renderListShoe()}</div>;
}
