import React from "react";

export default function ItemShoe({
  dataItem,
  handleViewDetail,
  handleAddToCart,
}) {
  return (
    <div className="col-6 text-center">
      <div className="card text-left">
        <img
          style={{ width: "200px", display: "block", margin: "auto" }}
          className="card-img-top"
          src={dataItem.image}
        />
        <div className="card-body text-center">
          <h5 className="card-title">{dataItem.name}</h5>
          <h4 className="card-text">{dataItem.price} $</h4>
          <button
            onClick={() => {
              handleViewDetail(dataItem);
            }}
            className="btn btn-warning"
          >
            View Detail
          </button>
          <button
            onClick={() => {
              handleAddToCart(dataItem);
            }}
            className="btn btn-success"
          >
            Add
          </button>
        </div>
      </div>
    </div>
  );
}
